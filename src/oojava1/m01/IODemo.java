package oojava1.m01;

import java.util.Scanner;

public class IODemo {

	public static void main(String[] args) {
		System.out.println("This println().");
		System.out.print("This just print()...");
		System.out.printf("\nThis is a String with and %s in it. And an integer: %d\t%d\t%d.\n"      ,       "sTrInG",42,43,44);
		System.out.print("This is the second print(). Now enter an intger: ");
		
	//  Class      Object reference       New Object created.
		Scanner     s                   = new Scanner(System.in);
		Scanner		t=null;
		t = s; // Does not create a new Scanner object, only creates a new reference to the same.
//		int n = s.nextInt();
		int n = t.nextInt();
		
		System.out.printf("We got this int from the keyboard: %d\n", n);

	}

}
