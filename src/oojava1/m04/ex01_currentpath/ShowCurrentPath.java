package oojava1.m04.ex01_currentpath;

public class ShowCurrentPath {
	
	public static void main(String[] args) {
		//https://stackoverflow.com/questions/4871051/how-to-get-the-current-working-directory-in-java
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
	}

	
}
