package oojava1.m04.ex06_exception;

public class NullPointerExceptionDemo {

	public static void main(String[] args) {
		IntContainer[] nn = new IntContainer[3];
		nn[0] = new IntContainer(100);
		nn[1] = new IntContainer(1000);
		
		for (int i = 0; i<nn.length; i++) {
			try {
				System.out.printf("Element content: %d%n", nn[i].value);
			}
			catch (NullPointerException e) {
				System.out.println("Exception caught: "+e.getMessage());
			}
		}
		
		for (int i = 0; i<nn.length; i++) {
			if (nn[i]!=null) 
				System.out.printf("Element content: %d%n", nn[i].value);
			else 
				System.out.println("Null pointer at element "+i);
		}

	}
}

class IntContainer{
	public int value;
	public IntContainer(int value) {
		this.value = value;
	}
}
